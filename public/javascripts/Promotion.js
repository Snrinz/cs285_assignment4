                                        /* Strategy Pattern */

function Service () {
    var price;

    return {
        set price (p) { this.price = p; } ,
        get price () { return price; }
    };    
}

function Breifcase () { this.price = 20; }
function Healthcare () { this.price = 100; }

function Package () {
    var servicelist;

    return {
        addService : function (service) {
            if(servicelist == null) servicelist = [Service];
            servicelist.push(service);
        } ,
        getAllService : function () {
            return servicelist;
        } ,
        getTotalPrice : function () {
            var totalprice = 0;
            for(var i = 1 ; i < servicelist.length ; i++){
                totalprice += servicelist[i].price;
            }
            return totalprice;
        } ,
        getPrivilage : function (promotion) {
            return promotion.getPrivilage(this);
        } ,
        containBreifcase : function () {
            for(var i in servicelist)
             if(servicelist[i] instanceof Breifcase) return true;

             return false;
        } ,
        containHealthcare : function () {
            for(var i in servicelist)
             if(servicelist[i] instanceof Healthcare) return true;
             
            return false;
        }
    };
    
}

function Promotion () {
    return {
        getPrivilage : function (package) { }
    };
}

function PromotionSet1 () {
    return {
        getPrivilage : function (package) {
            if(package.containBreifcase() && package.containHealthcare()){
                discount = 10;
                console.log(package.getTotalPrice());
                
                return package.getTotalPrice()/100*discount;
            }
            return false;
         }
    };
}
console.log(new Breifcase().price);

// subclass extend Service
Breifcase.prototype = Object.create(Service.prototype);
Healthcare.prototype = Object.create(Service.prototype);

// subclass extend Promotion
PromotionSet1.prototype = Object.create(Promotion.prototype);


var package = new Package();
package.addService(new Breifcase());
package.addService(new Healthcare());

console.log("ค่าบริการทั้งหมด : " + package.getTotalPrice());


console.log("Offer for promotion set1 : " + package.getPrivilage(new PromotionSet1()));

var jsonStr = '{"theTeam":[{"teamId":"1","status":"pending"},{"teamId":"2","status":"member"},{"teamId":"3","status":"member"}]}';

var obj = JSON.parse(jsonStr);
obj['theTeam'].push({"teamId":"4","status":"pending"});
jsonStr = JSON.stringify(obj);

console.log(jsonStr);
